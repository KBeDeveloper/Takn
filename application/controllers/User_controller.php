<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class User_controller extends CI_Controller {
	public function __construct(){
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Product_model');
        $this->load->model('Place_model');
    }
    
    public function closeSession(){
	    $this->session->set_userdata("id",null);
        $this->session->set_userdata("complete_name",null);
        $this->session->set_userdata("mail",null);
        $this->session->set_userdata("user_image",null);       
        $this->session->set_userdata("admin_prop",null);
        $this->session->set_userdata("cart", null);
	    $this->session->sess_destroy();
	    redirect('/Welcome', 'refresh');
    }

	public function loginPost(){
        $_SESSION['message'] = 'Error, el usuario o la contraseña son incorrectos.';
        $email     = $this->input->post('input-email');
        $password  = hash("sha256", $this->input->post('input-password'));
        $user      = $this->User_model->login($email, $password);  
        $cart_products = [];      
        if(count($user)==1){
            $this->session->set_userdata("id", $user[0]['T_USER_ID']);
            $this->session->set_userdata("complete_name", $user[0]['T_USER_FIRSTNAME']." ".$user[0]['T_USER_LASTNAME']);
            $this->session->set_userdata("mail", $user[0]['T_USER_EMAIL']);
            $this->session->set_userdata("user_image", $user[0]['T_USER_IMAGE']);
            $this->session->set_userdata("admin_prop", $user[0]['T_USER_TYPE']);
            $this->session->set_userdata("cart", $cart_products);
            redirect('/Welcome','refresh');
        }else{
            if(count($this->User_model->read($email))==1){
                /*if (isset($_SESSION['message'])){
                    echo $_SESSION['message'];
                    unset($_SESSION['message']);
                }*/
                $msg =  '<script type="text/javascript">
                            $("#button-login").append(
                                \'<p class="row text-center" style="color: red;">
                                    Error, contraseña incorrecta.
                                </p>\'
                            );
                        </script>';          
                redirect('/User_controller', $msg);
            }else{                
                redirect('/User_controller/register_page','refresh');
            }          
		}
    }
    
    public function register(){    
        try{
            if($this->session->id !== null){
                redirect('/welcome','refresh');
            }else{
                if(count($this->User_model->read($this->input->post('input-email')))==0){
                    $cellphone = null;
                    $phone     = null;
                    if(substr($this->input->post('input-type-phone'),3,1)=='9'){
                        $cellphone = '+569'.$this->input->post('input-phone');
                    }else{
                        $phone     = '+562'.$this->input->post('input-phone');
                    }
                    if($this->input->post('input-password')==$this->input->post('input-confirm-password')){
                        $data = [             
                            "T_USER_ID"          => substr(hash('sha256',$this->input->post('input-firstname')), 0, 7).substr(hash('sha256',$this->input->post('input-email')),0,8),           
                            "T_USER_FIRSTNAME"   => $this->input->post('input-firstname'),
                            "T_USER_LASTNAME"    => $this->input->post('input-lastname'),
                            "T_USER_IMAGE"       => 'https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.microsoft.com%2Fen-us%2Fresearch%2Fwp-content%2Fthemes%2Fmicrosoft-research-theme%2Fassets%2Fimages%2Fsvg%2Ficon-people-circle.svg&f=1',
                            "T_USER_EMAIL"       => $this->input->post('input-email'),
                            "T_USER_PHONE_1"     => $phone,
                            "T_USER_CELLPHONE_1" => $cellphone,
                            "T_USER_PASSWORD"    => hash('sha256',$this->input->post('input-password')),
                            "T_USER_TYPE"        => 2                
                        ];                        
                        $user = $this->User_model->create($data);                                                                      
                        redirect('/User_controller','refresh');                        
                    }else{
                        redirect('/User_controller/register','refresh');
                    }                     
                }else{
                    
                }
            }               
        }catch(Exception $e){       
            echo '<h1>Ha ocurrido un error en la aplicación. Por favor, inténtelo de nuevo más tarde.</h1>';
            echo '<p>Para regresar a la página anterior, <a href="<?=base_url()?>index.php/User_controller/register_page">presione aquí</a>.</p>';
        }
    }

	public function index(){
        if($this->session->id!==null){
            redirect('/User_controller/catalogue','refresh');
        }else{
            //$this->load->helper('default');
		    $this->load->view('login_view');
        }		
    }
    
    public function register_page(){
        if($this->session->id !== null){
            redirect('/User_controller/profiler');
        }else{
            //$this->load->helper('default');
            $this->load->view('register_view');
        }        
    }

    public function profiler(){
        if($this->session->id == null){
            redirect('/welcome','refresh');
        }else{            
            //$this->load->helper('main');
            $this->load->view('profile_view');
        }     
    }

    public function catalogue(){
        $products['products'] = $this->Product_model->getAll();
        //$this->load->helper('main');
        $this->load->view('catalogue_view', $products);        
    }

    public function cartle(){
        $products['orders'] = $this->session->cart;
        $products['products'] = [];
        foreach($products['orders'] as $ket => $o){
            $order = $this->Product_model->read($o[0]['ORDER_PRODUCT_PRODUCT_ID']);
            $products['products'] += $order;
        }
        $this->load->view('cart_view', $products);
    }

    public function places(){
        if($this->session->id == null){
            redirect('/welcome','refresh');
        }else{
            $places['places'] = $this->Place_model->read($this->session->id);
            //$this->load->helper('main');
            $this->load->helper('places_register');
            $this->load->view('Places_view', $places);
        }
    }

    public function register_place(){
        try{
            $depar = $this->input->post('input-place-department');        
            if(empty(trim($depar, ' '))){
                $depar = null;
            }
            $data = [
                'PLACE_NAME'           => $this->input->post('input-place-name'),
                'PLACE_ADDRESS'        => $this->input->post('input-place-address'),
                'PLACE_BUILDING_NUMBER'=> $this->input->post('input-place-building-number'),
                'PLACE_DEPARTMENT'     => $depar,
                'PLACE_REGION'         => $_POST['select-regions'],
                'PLACE_DISTRICT'       => $_POST['select-districts'],
                'PLACE_POSTAL_CODE'    => $this->input->post('input-place-postal-code'),
                'PLACE_T_USER_ID'      => $this->session->id
            ];            
            if(count($this->Place_model->read($this->session->id))>5){
                $this->session->set_flashdata('msj', 'No se ha podido registrar, el limite de registro es 5');                
            }else{
                $place = $this->Place_model->create($data);                     
            }
            redirect('/User_controller/places','refresh');   
        }catch(Exception $e){
            echo 'error';
        }
    }

    public function update_place($a_place){
        //$this->load->helper('main');
        $this->load->view('Places_view', $a_place);
    }

    public function orders(){
        if($this->session->id!=null){
            $this->load->model("Order_model");                
            $order['order'] =$this->Order_model->read($this->session->id);
            if($order['order'] != null)
                $order['names']= $this->Order_model->searchProduct($order['order'][0]['PRODUCT_ID']);

                $this->load->view('orders_view',$order);
                //var_dump($this->Order_model->searchProduct($order['order'][0]['PRODUCT_ID']));
        }else{
            $this->session->set_flashdata('msg', 'No has iniciado sesión');
            redirect('/User_controller', 'refresh');
        }
    }

    public function newsletter(){
        $this->User_model->createEmail($this->input->post('input-newsletter-email'));
        $this->load->view('index_view');
    }

    public function delComment(){
        $com = $this->input->post('input-comment-id');
        $pro = $this->input->post('input-product-id');
        $this->Product_model->deleteComment($com);
        $url = '/Product_controller/product/'.$pro;
        redirect($url, 'refresh');
    }
    public function editor(){
        if($this->session->id != null){
        $this->load->view('edit_view');
        }else{
            redirect('/welcome','refresh');
        }
    }

    public function modifier(){        
        $data = [                         
            "T_USER_FIRSTNAME"   => $this->input->post('input-firstname'),
            "T_USER_LASTNAME"    => $this->input->post('input-lastname'),
            "T_USER_IMAGE"       => $this->input->post('input-url'),
            "T_USER_EMAIL"       => $this->input->post('input-email'),
            "T_USER_PHONE_1"     => $_POST['input-phone'],
            "T_USER_CELLPHONE_1" => $_POST['input-phone-1'],
            "T_USER_PHONE_2"     => $_POST['input-phone-2'],
            "T_USER_CELLPHONE_2" => $_POST['input-phone-3']
        ];                        
        if($this->User_model->update($this->session->id,$data)){
            $this->session->set_userdata("complete_name", $data['T_USER_FIRSTNAME']." ".$data['T_USER_LASTNAME']);            
            $this->session->set_userdata("user_image", $data['T_USER_IMAGE']);
        redirect('User_controller/profiler','refresh');
        }else{
            redirect('/User_controller/editor','refresh');
        }
    }
}
?>
