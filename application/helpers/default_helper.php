<!-- Bootstrap CSS -->
<link href="<?=base_url()?>resources/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts -->
<link href="<?=base_url()?>resources/vendor/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css">
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic'
rel='stylesheet' type='text/css'>
<!-- Plugin CSS -->
<link href="<?=base_url()?>resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
<!-- Custom -->
<link href="<?=base_url()?>resources/css/creative.css" rel="stylesheet">

<script src="<?=base_url()?>resources/js/jquery.js"></script>

<script src="<?=base_url()?>resources/js/popper.js"></script>
<script src="<?=base_url()?>resources/js/bootstrap.js"></script>
<!-- Bootstrap core JavaScript -->
<script src="<?=base_url()?>resources/vendor/jquery/jquery.js"></script>
<script src="<?=base_url()?>resources/vendor/bootstrap/js/bootstrap.bundle.js"></script>

<!-- Plugin JavaScript -->
<script src="<?=base_url()?>resources/vendor/jquery-easing/jquery.easing.js"></script>
<script src="<?=base_url()?>resources/vendor/scrollreveal/scrollreveal.js"></script>
<script src="<?=base_url()?>resources/vendor/magnific-popup/jquery.magnific-popup.js"></script>

<!-- Custom scripts for this template -->
<script src="<?=base_url()?>resources/js/creative.js"></script>