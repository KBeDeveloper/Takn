<link href="<?=base_url()?>resources/vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
<!-- Custom fonts -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link href="<?=base_url()?>resources/css/fontawesome-all.css" rel="stylesheet">
<!-- Plugin CSS -->
<link href="<?=base_url()?>resources/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
<!-- Custom -->
<link href="<?=base_url()?>resources/css/creative.css" rel="stylesheet">


<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/all.js" integrity="sha384-xymdQtn1n3lH2wcu0qhcdaOpQwyoarkgLVxC/wZ5q7h9gHtxICrpcaSUfygqZGOe" crossorigin="anonymous"></script>
<!-- Bootstrap core JavaScript -->
<script src="<?=base_url()?>resources/vendor/jquery/jquery.min.js"></script>
<script src="<?=base_url()?>resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Plugin JavaScript -->
<script src="<?=base_url()?>resources/vendor/jquery-easing/jquery.easing.min.js"></script>
<script src="<?=base_url()?>resources/vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="<?=base_url()?>resources/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
<!-- Custom scripts for this template -->
<script src="<?=base_url()?>resources/js/creative.js"></script>