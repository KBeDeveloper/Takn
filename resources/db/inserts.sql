/*HASHES
    EMAIL    := b.esteban.calisto@gmail.com - SHA256 HASH := 99CE2C19A23AADBB8416AFEABE65E745E63B0D91D30073213238002DAD352952
    FRISTNAME:= Benjamin                    - SHA256 HASH := F3C7769DD45D51CBD33A2105124274B75DFB43ECC028D899C2313A4F2F0F95CE
    PASSWORD := penne                       - SHA256 HASH := 1339D4C96DD42DC13567686D7CB1DB7AE16F176A6D03327B799F81A4909A3C59
*/

INSERT INTO CLIENT VALUES 
(
    "99CE2C1F3C7769D", -- CONCAT 7 FIRST SHA265 HASH CHARS FROM EMAIL AND 8 FIRST SHA256 HASH CHARS FROM FIRSTNAME
    "Benjamin",
    "Calisto",
    "https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fwww.microsoft.com%2Fen-us%2Fresearch%2Fwp-content%2Fthemes%2Fmicrosoft-research-theme%2Fassets%2Fimages%2Fsvg%2Ficon-people-circle.svg&f=1",
    "b.esteban.calisto@gmail.com",
    null,
    null,
    74985122,
    null,
    "1339D4C96DD42DC13567686D7CB1DB7AE16F176A6D03327B799F81A4909A3C59" --penne as SHA256
);

INSERT INTO PRODUCT 
VALUES
(
    1,
    "https://atmedia.imgix.net/33e71e0eabb68a2509ce13916db91d2aa220d2ea?auto=format&q=45&w=640.0&h=430.0&fit=max&cs=strip", 
    "Rollos de repollo", 
    "Pastelillo salado", 
    "El repollo relleno es un plato consistente en hojas de repollo envueltas alrededor de diversos rellenos.", 
    "Rumania", 
    "3890", 
    "Disponible"
),

(
    2,
    "http://40aprons.com/wp-content/uploads/2016/01/bacon-ramen-6.jpg", 
    "Ramen de Pato Tradicional", 
    "Sopa de fideos", 
    "Sopa de fideos de trigo, con pato agridulce, caldo de grasa y una mezcla vegetales salteados.", 
    "China", 
    "4290",
    "Disponible"
),
(
    3,
    "http://www.lebanquet.cl/wp-content/uploads/2017/09/risotto-en-baja.jpg", 
    "Risotto de Setas", 
    "Pasta de arroz", 
    "Arroz finamente seleccionado, cocinado al vapor con vino blanco, exquisitas especias, queso bianco di parma y una variedad de setas que cautivará tu paladar.", 
    "Italia", 
    "3890", 
    "Disponible"
),
(
    4,
    "https://cleanfoodcrush.com/wp-content/uploads/2017/08/Eat-Clean-Classic-Summertime-Ratatouille--1024x683.jpg", 
    "Ratatouille", 
    "Guisado de vegetales", 
    "Pastel de verduras guisadas al horno. Una selección de los mejores vegetales a la francesa.", 
    "Francia", 
    "3340", 
    "Disponible"
),
(
    5,
    "https://www.viajejet.com/wp-content/viajes/Salchichas-de-Alemania-1440x810.jpg", 
    "Salchicha Bábara", 
    "Bocadillo salado", 
    "Salchicha bábara tradicional, con mostaza Dijon y chukrut fresco, encapsulado en un crujiente pan baguette", 
    "Alemania", 
    "1890", 
    "Disponible"
);